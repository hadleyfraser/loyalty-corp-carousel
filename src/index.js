import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './components/App/App';
import registerServiceWorker from './registerServiceWorker';
import './index.scss';

const render = Component => {
  ReactDOM.render(
  <AppContainer>
    <App />
  </AppContainer>,
  document.getElementById('root')
  );
};

registerServiceWorker();

render(App);

if (module.hot) {
  module.hot.accept('./components/App/App', () => {
  render(App);
  });
}