import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Swiper from 'react-id-swiper';
import Carousel from './Carousel';

describe('<Carousel>', () => {
  const wrapper = shallow(<Carousel defaultNoSlides={3} />);
  const instance = wrapper.instance();
  it('will render a swiper', () => {
    expect(wrapper.find(Swiper).length).toBe(1);
  });
});
