import React from 'react';
import Swiper from 'react-swiper-id';
import PropTypes from 'prop-types';
import { getSlidesPerView } from '../../utils/utils';
import './Carousel.scss';

class Carousel extends React.Component {
  state = {
    slidesPerView: getSlidesPerView(this.props.sizes, this.props.defaultNoSlides),
  }

  componentDidMount() {
    const { sizes, defaultNoSlides } = this.props;
    window.addEventListener('resize', () => {
      this.setState({ slidesPerView: getSlidesPerView(sizes, defaultNoSlides) });
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.slidesPerView !== nextState.slidesPerView;
  }

  prevSlide = () => {
    if (this.carousel && this.carousel.swiper) {
      this.carousel.swiper.slidePrev();
    }
  }

  nextSlide = () => {
    if (this.carousel && this.carousel.swiper) {
      this.carousel.swiper.slideNext();
    }
  }

  render() {
    return (
      <div className="carousel">
        <a className="carousel__nav carousel__nav--prev" onClick={this.prevSlide}><span>&laquo;</span></a>
        <Swiper
          ref={(s) => { this.carousel = s; }}
          containerClass="carousel__swiper"
          slidesPerView={this.state.slidesPerView}
          loop
        >
          {React.Children.map(this.props.children, (child, i) => {
            return (
              <div className="carousel__slide">{child}</div>
            )
          })}
        </Swiper>
        <a className="carousel__nav carousel__nav--next" onClick={this.nextSlide}><span>&raquo;</span></a>
      </div>
    )
  }
}

Carousel.propTypes = {
  defaultNoSlides: PropTypes.number.isRequired,
  sizes: PropTypes.object,
};

Carousel.defaultProps = {
    sizes: {},
};

export default Carousel;