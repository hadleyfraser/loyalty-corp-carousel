import React from 'react';
import './Header.scss';

const Header = ({children}) => (
  <nav>
    <div className="nav-container">
      {children}
    </div>
  </nav>
);

export default Header;