import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Header from './Header';

describe('<Header> is rendered with 3 children', () => {
  it('will have rendered 3 a tags', () => {
    const wrapper = shallow(<Header><a>1</a><a>1</a><a>1</a></Header>);
    expect(wrapper.find('a').length).toBe(3);
  });
});
