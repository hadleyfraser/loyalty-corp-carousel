import React, { Component } from 'react';
import Header from '../Header/Header';
import Carousel from '../Carousel/Carousel';
import './App.scss';

class App extends Component {
  render() {
    // setup images to be rendered in the carousel
    const images = [];
    for (let i = 1; i <= 6; i+=1) {
      images.push(<img src={`./assets/${i}.jpg`} alt="Pug" key={i} />)
    }

    return (
      <div className="app">
        <Header>
          <a href="#">Home</a>
          <a href="#">About Us</a>
          <a href="#">Contact Us</a>
        </Header>
        <Carousel
          defaultNoSlides={3}
          sizes={{
            768: 1,
            1024: 2,
          }}
        >
          {images}
        </Carousel>
        <div className="app__spacer" />
      </div>
    );
  }
}

export default App;
