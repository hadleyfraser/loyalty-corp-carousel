import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import App from './App';
import Header from '../Header/Header';
import Carousel from '../Carousel/Carousel';

describe('when <App> is rendered', () => {
  const wrapper = shallow(<App />);
  it('will render a Header element', () => {
    expect(wrapper.find(Header).length).toBe(1);
  });
  it('will render a Carousel element', () => {
    expect(wrapper.find(Carousel).length).toBe(1);
  });
});
