/**
 * Returns the number of slides that should be visible depending on the sizes
 * that are passed in
 * 
 * @param {{}} sizes Object of sizes in format {size: noOfSlides}
 * @param {number} defaultNoSlides If no size found use this value
 * 
 * @return {number}
 */
const getSlidesPerView = (sizes, defaultNoSlides) => {
  const winWidth = window.innerWidth;
  const screenSizes = {...sizes};

  const selectedSize = Math.min(...Object.keys(screenSizes)
      .map((size) => parseInt(size, 10))
      .filter(size => size >= winWidth));

  return sizes[selectedSize] ? sizes[selectedSize] : defaultNoSlides;
};

export {
  getSlidesPerView
};