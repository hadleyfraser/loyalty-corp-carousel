import { getSlidesPerView } from './utils';

describe('getSlidesPerView()', () => {
  const screenSizes = {
    1024: 2,
    768: 1,
  };
  const defaultNoSlides = 3;

  describe('when the screen is 1500 wide', () => {
    it(`will return ${defaultNoSlides}`, () => {
      window.innerWidth = 1500;
      const result = getSlidesPerView(screenSizes, defaultNoSlides);
      expect(result).toBe(3);
    });
  });

  describe('when the screen is 1000 wide', () => {
    it(`will return ${defaultNoSlides}`, () => {
      window.innerWidth = 1000;
      const result = getSlidesPerView(screenSizes, defaultNoSlides);
      expect(result).toBe(2);
    });
  });

  describe('when the screen is 500 wide', () => {
    it(`will return ${defaultNoSlides}`, () => {
      window.innerWidth = 500;
      const result = getSlidesPerView(screenSizes, defaultNoSlides);
      expect(result).toBe(1);
    });
  });
});
